//Fill the table body.
//Grab storage, process it, add listeners.

var GLOBAL_workingHostname;

var tableBody = document.getElementById("table-data-segment");
var cssEditArea = document.getElementById("css-editing-textarea");
var cssTextareaLabel = document.getElementById("css-textarea-label");
var cssSubmitButton = document.getElementById("set-css-button");
var restoreDefaultButton = document.getElementById("restore-default-button");
var importTextarea = document.getElementById("import-storage-textarea");
var importButton = document.getElementById("additive-import-button");
var exportTextarea = document.getElementById("export-storage-textarea");

var setTableHtml = async function() {

    var objectStorage = await chrome.storage.local.get(null);
    var normalizedObject = storageToNormalizedObject(objectStorage);

    tableBody.innerHTML = Object.keys(normalizedObject).map((key) => `
<tr id="${key}-table-row">
    <td><button id="${key}-delete-button">X</button></td>
    <td>${key}</td>
    <td>${normalizedObject[key].exempt}</td>
    <td><div>${normalizedObject[key].css}</div></td>
</tr>`
        ).join('');

    Array.from(tableBody.rows).forEach(row => row.addEventListener("click", selectRow));
    Array.from(tableBody.rows).forEach(row => row.children[0].children[0].addEventListener("click", deleteDomain));

    exportTextarea.innerHTML = JSON.stringify(objectStorage);

};

var storageToNormalizedObject = function(objectStorage) {
    var normalizedObject = {};
    Object.entries(objectStorage).forEach(([key, value]) => {

        domainInQuestion = key.replace("_exempt", '');

        normalizedObject[domainInQuestion] ??= {};

        if (key.endsWith("_exempt")) {
            normalizedObject[domainInQuestion].exempt = value;
        } else {
            normalizedObject[domainInQuestion].css = value;
        }

    });
    return normalizedObject;
};

var deleteDomain = function() {
    var hostname = this.id.replace("-delete-button", '');
    chrome.storage.local.remove([hostname, `${hostname}_exempt`]);
    setTableHtml();
};

var selectRow = async function() {
    var hostname = this.id.replace("-table-row", '');
    var objectStore = await chrome.storage.local.get(hostname);
    GLOBAL_workingHostname = hostname;
    cssTextareaLabel.innerHTML = `Style for ${hostname}`;
    cssEditArea.innerHTML = objectStore[hostname];
    cssEditArea.scrollIntoView({behavior: "smooth", block: "center"});
};

var setCssForSite = async function() {
    chrome.storage.local.set({[GLOBAL_workingHostname]: cssEditArea.value});
    setTableHtml();
};

var restoreOriginalDefault = function() {
    chrome.runtime.sendMessage({message: "restore-default-css"});
    setTableHtml();
};

var additiveImport = function() {
    chrome.storage.local.set(JSON.parse(importTextarea.value));
    setTableHtml();
};

setTableHtml();
cssSubmitButton.addEventListener("click", setCssForSite);
restoreDefaultButton.addEventListener("click", restoreOriginalDefault);
importButton.addEventListener("click", additiveImport);

//This attaches to the page.
//We do the initial setting of the css content.
//Then we inject the style tag.
//Then we just listen for storage changes.
//Setting css content is always done according to whether we're exempt or not.
//And if the css is unset, we load the default.
//We also have to be around to tell people what our hostname is.

var hostname = document.location.hostname;
var newStyleTag = document.createElement("style");
newStyleTag.id = "dark-mode-custom-css-extension";

var setCssContent = async function() {
    var objectStore = await chrome.storage.local.get(null);
    if (objectStore[`${hostname}_exempt`]) {
        newStyleTag.innerHTML = "";
    } else {
        newStyleTag.innerHTML = objectStore[hostname] ?? objectStore["default"];
    }
};

setCssContent();
document.documentElement.appendChild(newStyleTag);

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if (request.message === "query-hostname") {

            sendResponse({payload: hostname});

        } else if (request.message === "toggle-exempt") {

            var hostnameStatusKey = `${hostname}_exempt`;
            chrome.storage.local.get(null, function(objectStore) {
                chrome.storage.local.set({[hostnameStatusKey]: !objectStore[hostnameStatusKey]});
            });

        }
    });

chrome.storage.onChanged.addListener(() => setCssContent());

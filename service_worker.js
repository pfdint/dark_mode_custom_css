//This service worker listens for keyboard shortcuts
//This service worker also sets the default on install

var originalDefaultCss =
`html {
    background: #000 !important;
}

body {
    background-color: #fff !important;
    filter: invert(.95) brightness(.7) sepia(.3) !important;
}

img,
video,
audio,
embed,
iframe,
object,
*[class*="icon"],
*[class*="Icon"],
*[style*="background-image"],
*[style*="background-position"],
*[style*="background-repeat"],
*[style*="url("] {
    filter: invert(1) !important;
}

/* width */
::-webkit-scrollbar {
    width: 20px;
}

/* track */
::-webkit-scrollbar-track {
    background: #777;
}

/* handle */
::-webkit-scrollbar-thumb {
    background: #444;
}

/* handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #555;
}`;

var setDefaultCssInStorage = async function() {
    var objectStore = await chrome.storage.local.get(["default"]);
    if (objectStore["default"] === undefined) {
        chrome.storage.local.set({"default": originalDefaultCss});
    }
};

var queryForCurrentTabId = async function() {
    var tabs = await chrome.tabs.query({active: true, currentWindow: true});
    return tabs[0].id;
}

//Turns out this runs more often than you think.
//This runs every time chromium updates.
chrome.runtime.onInstalled.addListener(setDefaultCssInStorage);

chrome.commands.onCommand.addListener(
    async function(command) {
        if (command === "toggle-exempt") {
            var tabId = await queryForCurrentTabId();
            chrome.tabs.sendMessage(tabId, {message: "toggle-exempt"});
        } 
    });

chrome.runtime.onMessage.addListener(
    function(request) {
        if (request.message === "restore-default-css") {
            chrome.storage.local.set({"default": originalDefaultCss});
        }
    });

//The popup's logic.
//Every time we show, we change the label to show what domain we're working on.
//Every time we show, we put the old css into the textarea.
//Every time the button is clicked, we save our css.

var queryForCurrentTabId = async function() {
    var tabs = await chrome.tabs.query({active: true, currentWindow: true});
    return tabs[0].id;
}

var queryForTabHostname = async function() {
    var tabId = await queryForCurrentTabId();
    var response = await chrome.tabs.sendMessage(tabId, {message: "query-hostname"});
    return response.payload;
};

var replaceDomainLabel = async function() {
    try {
        var hostname = await queryForTabHostname();
        domainLabel.innerHTML = `Style for ${hostname}`;
    } catch {
        domainLabel.innerHTML = "Please reload this tab before using this extension on it.";
    }
};

var displayCurrentCss = async function() {
    try {
        var hostname = await queryForTabHostname()
        var objectStore = await chrome.storage.local.get(null);
        cssTextarea.innerHTML = objectStore[hostname] ?? objectStore["default"];
    } catch {
        cssTextarea.innerHTML = "Please reload.";
    }
}

var saveCssForSite = async function() {
    try {
        var hostname = await queryForTabHostname();
        chrome.storage.local.set({[hostname]: cssTextarea.value});
    } catch(error) {
        domainLabel.innerHTML = `Couldn't save CSS. Preserve your entry and reload the tab.`;
        console.log(error);
    }
};

var domainLabel = document.getElementById("domain-label");
var cssTextarea = document.getElementById("css-input-textarea");

document.addEventListener("DOMContentLoaded", replaceDomainLabel);
document.addEventListener("DOMContentLoaded", displayCurrentCss);
document.getElementById("input-complete-button").addEventListener("click", saveCssForSite);
